/*
 * @author Bilaal Rashid
 * Student Number: 991521985
 */

package password;

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidatorTest {

	// isValidLength Regular Test
	@Test
	public void testIsValidLengthRegular() {
		assertTrue("Password has an invalid length", PasswordValidator.isValidLength("1234567890"));
	}

	// isValidLength Exception Test
	@Test
	public void testIsValidLengthException() {
		assertFalse("Password has an invalid length", PasswordValidator.isValidLength(null));
	}

	// isValidLength Exception Spaces Test
	@Test
	public void testIsValidLengthExceptionSpaces() {
		assertFalse("Password has an invalid length", PasswordValidator.isValidLength("          "));
	}

	// isValidLength BoundaryIn Test
	@Test
	public void testIsValidLengthBoundaryIn() {
		assertTrue("Password has an invalid length", PasswordValidator.isValidLength("12345678"));
	}

	// isValidLength BoundaryOut Test
	@Test
	public void testIsValidLengthBoundaryOut() {
		assertFalse("Password has an invalid length", PasswordValidator.isValidLength("1234567"));
	}

	// hasEnoughDigits Regular test
	@Test
	public void testHasEnoughDigitsRegular() {
		assertTrue("Password dosen't have enough digits", PasswordValidator.hasEnoughDigits("abcdefgh123"));
	}

	// hasEnoughDigits Exception test
	@Test
	public void testHasEnoughDigitsException() {
		assertFalse("Password dosen't have enough digits", PasswordValidator.hasEnoughDigits(null));
	}

	// hasEnoughDigits BoundaryIn test
	@Test
	public void testHasEnoughDigitsBoundaryIn() {
		assertTrue("Password dosen't have enough digits", PasswordValidator.hasEnoughDigits("abcdefgh12"));
	}

	// hasEnoughDigits BoundaryOut test
	@Test
	public void testHasEnoughDigitsBoundaryOut() {
		assertFalse("Password dosen't have enough digits", PasswordValidator.hasEnoughDigits("abcdefgh1"));
	}

	// hasValidCaseChars Regular test
	@Test
	public void testHasValidCaseCharsRegular() {
		boolean result = PasswordValidator.hasValidCaseChars("aAaAaAaA");
		assertTrue("Invalid case characters", result);
	}

	// hasValidCaseChars Exception test
	@Test
	public void testHasValidCaseCharsException() {
		boolean result = PasswordValidator.hasValidCaseChars("8989898");
		assertFalse("Invalid case characters", result);
	}

	// hasValidCaseChars Boundary In Lower test
	@Test
	public void testHasValidCaseCharsBoundaryInLower() {
		boolean result = PasswordValidator.hasValidCaseChars("aA");
		assertTrue("Invalid case characters", result);
	}

	// hasValidCaseChars Boundary Out Lower test
	@Test
	public void testHasValidCaseCharsBoundaryOutLower() {
		boolean result = PasswordValidator.hasValidCaseChars("aaaaa");
		assertFalse("Invalid case characters", result);
	}

	// hasValidCaseChars Boundary Out Upper test
	@Test
	public void testHasValidCaseCharsBoundaryOutUpper() {
		boolean result = PasswordValidator.hasValidCaseChars("AAAA");
		assertFalse("Invalid case characters", result);
	}

}
