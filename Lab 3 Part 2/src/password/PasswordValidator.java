/*
 * @author Bilaal Rashid
 * Student Number: 991521985
 * 
 * This class validates passwords and it will developed using 
 */

package password;

import java.util.Iterator;

public class PasswordValidator {

	private static int MIN_LENGTH = 8;
	private static int MIN_NUM_OF_DIGITS = 2;

	/*
	 * Check if password is more then 8 characters Check that it is not null Removes spaces
	 */
	public static boolean isValidLength(String password) {
		if (password != null) {
			return password.trim().length() >= MIN_LENGTH;
		}
		return false;
	}

	/*
	 * Check if password has 2 or more digits Check that it is not null
	 */
	public static boolean hasEnoughDigits(String password) {
		int numOfNumbers = 0;

		if (password != null) {
			for (int i = 0; i < password.length(); i++) {
				if (Character.isDigit(password.charAt(i))) {
					numOfNumbers++;
					if (numOfNumbers == 2) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/*
	 * Check if password has a uppercase
	 */

	public static boolean hasValidCaseChars(String password) {
		return password != null && password.matches(".*[A-Z]+.*") && password.matches(".*[a-z]+.*");
	}
}
